package domaci7.coupon.rest;

import domaci7.coupon.domain.Coupon;
import domaci7.coupon.service.CouponService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/coupon")
public class CouponController {

    private final CouponService couponService = new CouponService();

    public CouponController() {
        
    }

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Coupon save(Coupon coupon) {
        return couponService.save(coupon);
    }

    @DELETE
    @Path("/delete/{id}")
    public void deleteById(@PathParam("id") Long id) {
        couponService.deleteById(id);
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Coupon findById(@PathParam("id") Long id) {
        return couponService.findById(id)
                .orElseThrow(IllegalArgumentException::new);
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Coupon> findAll() {
        return couponService.findAll();
    }

}
