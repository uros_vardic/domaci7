package domaci7.coupon.service;

import domaci7.coupon.domain.Coupon;
import domaci7.coupon.repository.CouponRepository;

import java.util.List;
import java.util.Optional;

public class CouponService {

    public Coupon save(Coupon coupon) {
        return CouponRepository.save(coupon);
    }

    public void deleteById(Long id) {
        CouponRepository.deleteById(id);
    }

    public Optional<Coupon> findById(Long id) {
        return CouponRepository.findById(id);
    }

    public List<Coupon> findAll() {
        return CouponRepository.findAll();
    }

}
