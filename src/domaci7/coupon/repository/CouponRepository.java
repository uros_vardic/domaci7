package domaci7.coupon.repository;

import domaci7.coupon.domain.Coupon;
import domaci7.shop.repository.ShopRepository;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CouponRepository {

    private static final Random random = new Random();

    private static long nextId;

    private static List<Coupon> coupons;

    static {
        coupons = generateCoupons();
    }

    private static List<Coupon> generateCoupons() {
        return IntStream.range(0, 10)
                .mapToObj(i -> generateCoupon())
                .collect(Collectors.toList());
    }

    private static Coupon generateCoupon() {
        Coupon coupon = new Coupon();

        coupon.setId(nextId++);
        coupon.setShop(ShopRepository.findById(ThreadLocalRandom.current().nextLong(0, 3))
                .orElseThrow(IndexOutOfBoundsException::new));
        coupon.setProduct("Product - " + nextId);
        coupon.setOriginalPrice(random.nextFloat() * (1000 - 100) + 100); // (max - min) + min
        coupon.setDiscountedPrice(random.nextFloat() * (coupon.getOriginalPrice() - 100) + 100);

        return coupon;
    }

    public static synchronized Coupon save(Coupon coupon) {
        coupon.setId(nextId++);
        coupons.add(coupon);

        return coupon;
    }

    public static synchronized void deleteById(Long id) {
        coupons.remove(Math.toIntExact(id));
    }

    public static synchronized Optional<Coupon> findById(Long id) {
        try {
            return Optional.of(coupons.get(Math.toIntExact(id)));
        } catch (Exception ignored) {}

        return Optional.empty();
    }

    public static synchronized List<Coupon> findAll() {
        return coupons;
    }

}
