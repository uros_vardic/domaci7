package domaci7.shop.domain;

import java.io.Serializable;

public class Shop implements Serializable {

    private Long id;

    private String name;

    public Shop() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
