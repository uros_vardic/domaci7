package domaci7.shop.repository;

import domaci7.shop.domain.Shop;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ShopRepository {

    private static List<Shop> shops;

    private static long nextId;

    static {
        shops = generateShops();
    }

    private static List<Shop> generateShops() {
        return IntStream.range(0, 3)
                .mapToObj(i -> generateShop())
                .collect(Collectors.toList());
    }

    private static Shop generateShop() {
        Shop shop = new Shop();

        shop.setId(nextId++);
        shop.setName("Shop - " + nextId);

        return shop;
    }

    public static synchronized Shop save(Shop shop) {
        shop.setId(nextId++);
        shops.add(shop);

        return shop;
    }

    public static synchronized void deleteById(Long id) {
        shops.remove(Math.toIntExact(id));
    }

    public static synchronized Optional<Shop> findById(Long id) {
        try {
            return Optional.of(shops.get(Math.toIntExact(id)));
        } catch (Exception ignored) {}

        return Optional.empty();
    }

    public static synchronized List<Shop> findAllByName(String name) {
        return shops.stream()
                .filter(shop -> shop.getName().equals(name))
                .collect(Collectors.toList());
    }

    public static synchronized List<Shop> findAll() {
        return shops;
    }

}
