package domaci7.shop.service;

import domaci7.shop.domain.Shop;
import domaci7.shop.repository.ShopRepository;

import java.util.List;
import java.util.Optional;

public class ShopService {

    public Shop save(Shop shop) {
        return ShopRepository.save(shop);
    }

    public void deleteById(Long id) {
        ShopRepository.deleteById(id);
    }

    public Optional<Shop> findById(Long id) {
        return ShopRepository.findById(id);
    }

    public List<Shop> findAllByName(String name) {
        return ShopRepository.findAllByName(name);
    }

    public List<Shop> findAll() {
        return ShopRepository.findAll();
    }

}
