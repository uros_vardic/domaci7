package domaci7.shop.rest;

import domaci7.shop.domain.Shop;
import domaci7.shop.service.ShopService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/shop")
public class ShopController {

    private final ShopService shopService = new ShopService();

    public ShopController() {}

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Shop save(Shop shop) {
        return shopService.save(shop);
    }

    @DELETE
    @Path("/delete/{id}")
    public void deleteById(@PathParam("id") Long id) {
        shopService.deleteById(id);
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Shop findById(@PathParam("id") Long id) {
        return shopService.findById(id)
                .orElseThrow(IllegalArgumentException::new);
    }

    @GET
    @Path("/name/{shopName}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Shop> findAllByName(@PathParam("shopName") String shopName) {
        return shopService.findAllByName(shopName);
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Shop> findAll() {
        return shopService.findAll();
    }

}
