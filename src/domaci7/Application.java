package domaci7;

import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/service")
public class Application extends ResourceConfig {

    public Application() {
        packages("domaci7");
    }

}
