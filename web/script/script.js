'use strict';

window.onload = function () {
    loadCoupons();
    loadShops();
};

function loadCoupons() {
    const xHttp = new XMLHttpRequest();

    xHttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            const result = JSON.parse(this.response);
            const table = document.getElementById("couponTable");
            const oldTBody = table.tBodies[0];
            const newTBody = document.createElement("tBody");

            for (let i = 0; i < result.length; i++) {
                const bRow = document.createElement("tr");
                const tdProduct = document.createElement("td");
                const tdShop = document.createElement("td");
                const tdDiscountedPrice = document.createElement("td");
                const tdOriginalPrice = document.createElement("td");
                const tdPercentage = document.createElement("td");
                const tdDelete = document.createElement("td");

                tdProduct.innerHTML = result[i].product;
                tdShop.innerHTML = result[i].shop.name;
                tdDiscountedPrice.innerHTML = Math.round(result[i].discountedPrice * 100) / 100;
                tdDiscountedPrice.className = "green-row";
                tdOriginalPrice.innerHTML = Math.round(result[i].originalPrice * 100) / 100;
                tdOriginalPrice.className = "red-row";
                const percentage = ((result[i].originalPrice - result[i].discountedPrice) / result[i].originalPrice) * 100;
                tdPercentage.innerHTML = Math.round(percentage * 100) / 100 + " %";
                const button = document.createElement("input");
                button.type = "button";
                button.value = " X ";
                button.setAttribute("onclick", "deleteRow(this)");
                tdDelete.appendChild(button);

                bRow.appendChild(tdProduct);
                bRow.appendChild(tdShop);
                bRow.appendChild(tdDiscountedPrice);
                bRow.appendChild(tdOriginalPrice);
                bRow.appendChild(tdPercentage);
                bRow.appendChild(tdDelete);

                newTBody.appendChild(bRow)
            }

            table.replaceChild(newTBody, oldTBody)
        }
    };

    xHttp.open("GET", "/service/coupon/all", true);
    xHttp.send();
}

function deleteRow(button) {
    const parentRowIndex = button.parentNode.parentNode.rowIndex - 1;

    const xHttp = new XMLHttpRequest();

    xHttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 204) {
            document.getElementById("couponTable").deleteRow(parentRowIndex + 1);
        }
    };

    xHttp.open("DELETE", "/service/coupon/delete/" + parentRowIndex, true);
    xHttp.send();
}

function loadShops() {
    const xHttp = new XMLHttpRequest();

    xHttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            const result = (JSON.parse(this.response));
            const select = document.getElementById("shopName");

            for (let i = 0; i < result.length; i++) {
                const option = document.createElement("option");

                option.text = result[i].name;
                option.value = result[i].name;

                select.add(option, 0);
            }
        }
    };

    xHttp.open("GET", "/service/shop/all", true);
    xHttp.send();
}

function processForm(e) {
    if (e.preventDefault) {
        e.preventDefault();
    }

    const formData = new FormData(e.target);

    const product = formData.get("product");
    const shopName = formData.get("shopName");
    const originalPrice = formData.get("originalPrice");
    const discountedPrice = formData.get("discountedPrice");

    const xHttp = new XMLHttpRequest();

    xHttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            const shop = JSON.parse(this.response)[0];
            saveCoupon(product, shop, originalPrice, discountedPrice);
        }
    };

    xHttp.open("GET", "/service/shop/name/" + shopName);
    xHttp.send();

    return false;
}

function saveCoupon(product, shop, originalPrice, discountedPrice) {
    const xHttp = new XMLHttpRequest();

    xHttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            loadCoupons();
        }
    };

    xHttp.open("POST", "/service/coupon/save", true);
    xHttp.setRequestHeader("Content-Type", "application/json");
    xHttp.send(JSON.stringify({product: product, shop: shop, originalPrice: originalPrice,
        discountedPrice: discountedPrice}));
}

const form = document.getElementById("form");

if (form.attachEvent) {
    form.attachEvent("submit", processForm);
} else {
    form.addEventListener("submit", processForm);
}
